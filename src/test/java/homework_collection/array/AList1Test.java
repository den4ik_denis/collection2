package test.java.homework_collection.array;

import com.java.homework_collection.array.AList1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AList1Test {
    private final AList1 cut = new AList1();

    static Arguments[] constructorsTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new AList1(), new int[]{}, 0),
                Arguments.arguments(new AList1(0), new int[]{}, 0),
                Arguments.arguments(new AList1(5), new int[]{}, 0),
                Arguments.arguments(new AList1(new int[]{1, 2, 3}), new int[]{1, 2, 3}, 3),
        };
    }


    @ParameterizedTest
    @MethodSource("constructorsTestArgs")
    void constructorTest(AList1 cut, int[] expected, int expSize) {
        int[] actual = cut.toArray();
        int actSize = cut.size();

        assertEquals(expSize, actSize);
        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void constructorsExceptionTest() {
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> new AList1(-2));
    }



    static Arguments[] toArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(2, new int[]{0, 1}),
                Arguments.arguments(5, new int[]{0, 1, 2, 3, 4}),
                Arguments.arguments(0, new int[]{}),
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int countAdd, int[] expected) {
        for (int i = 0; i < countAdd; i++) {
            cut.add(i);
        }
        int[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void clearTest() {
        int expSize = 0;
        for (int i = 0; i < 5; i++) {
            cut.add(i);
        }

        cut.clear();

        assertEquals(expSize, cut.size());
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> cut.get(0));
    }

    static Arguments[] addIndexTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1, 128),
                Arguments.arguments(5, 5, 355),
        };
    }

    @ParameterizedTest
    @MethodSource("addIndexTestArgs")
    void addIndexTest(int countOfAdd, int index, int expected) {

        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }

        cut.add(index-1, expected);
        int actual = cut.get(index-1);

        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(countOfAdd + 1, cut.size());
    }

    static Arguments[] addIndexExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(10, 6, false, 10),
                Arguments.arguments(8, 6, true, 10),
                Arguments.arguments(11, 6, false, 10),
        };
    }

    @ParameterizedTest
    @MethodSource("addIndexExceptionTestArgs")
    void addTest(int index,  int value, boolean expected, int countOfAdd) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }

        Assertions.assertEquals(expected, cut.add(index, value));
    }

    static Arguments[] containsTestArgs() {
        return new Arguments[]{
                Arguments.arguments(true, 2, 5),
                Arguments.arguments(false, 6, 5),
                Arguments.arguments(false, 0, 0),
        };
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(boolean expected, int value, int countOfAdd) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }

        boolean actual = cut.contains(value);

        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] removeByIndexTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1),
                Arguments.arguments(5, 4),
                Arguments.arguments(5, 0),
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int countOfAdd, int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }
        int expRemovedValue = index;

        int actRemovedValue = cut.removeByIndex(index);

        Assertions.assertEquals(expRemovedValue, actRemovedValue);
    }

    static Arguments[] removeByIndexExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 6),
                Arguments.arguments(5, -1),
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexExceptionTestArgs")
    void removeByIndexExceptionTest(int countOfAdd,  int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }

        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> cut.removeByIndex(index));
    }


    static Arguments[] removeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1),
                Arguments.arguments(5, 4),
        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int countOfAdd, int value) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }
        int expSize = cut.size() - 1;


        int actRemovedValue = cut.remove(value);
        int actSize = cut.size();


        Assertions.assertEquals(value, actRemovedValue);
        Assertions.assertEquals(expSize, actSize);
    }

    @Test
    void removeExceptionTest() {

        Assertions.assertThrows(NoSuchElementException.class, () -> cut.remove(222));

        cut.add(111);

        Assertions.assertThrows(NoSuchElementException.class, () -> cut.remove(222));
    }


    static Arguments[] setTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1, 555, true),
                Arguments.arguments(5, 0, 555, true),
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int countOfAdd, int index, int value, boolean expected) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }
        int expSize = cut.size();
        boolean actualRez = cut.set(index, value);


        int actual = cut.get(index);
        int actSize = cut.size();

        Assertions.assertEquals(expSize, actSize);
        Assertions.assertEquals(value, actual);
        Assertions.assertEquals(expected, actualRez);
    }

    @Test
    void setFalseTest() {

        Assertions.assertFalse(cut.set(0, 100));

        cut.add(3);
        cut.add(5);
        cut.add(8);

        Assertions.assertFalse(cut.set(3, 11));
    }

    static Arguments[] removeAllTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, new int[]{3, 1}, new int[]{0, 2, 4}),
                Arguments.arguments(5, new int[]{6, 1}, new int[]{0, 2, 3, 4}),
                Arguments.arguments(5, new int[]{6, 99}, new int[]{0, 1, 2, 3, 4}),
                Arguments.arguments(5, new int[]{}, new int[]{0, 1, 2, 3, 4}),
                Arguments.arguments(5, null, new int[]{0, 1, 2, 3, 4}),
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(int countOfAdd, int[] removeValues, int[] expected) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(i);
        }

        cut.removeAll(removeValues);

        Assertions.assertArrayEquals(expected, cut.toArray());
    }

    @Test
    void removeAllRepeatedValueTest() {
        cut.add(1);
        cut.add(2);
        cut.add(2);
        cut.add(3);
        int[] expected = new int[]{2, 3};

        cut.removeAll(new int[]{2, 1});

        Assertions.assertArrayEquals(expected, cut.toArray());
    }
}
